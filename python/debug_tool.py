#!/usr/bin/env python
# -*- coding: utf-8 -*-

__doc__ = """Tool for 'printing objects', 
    wich helps to debug code, when IDE is not availible"""

from pprint import pprint


def print_obj(object_name):
    try:
        pprint({x: getattr(object_name, x) for x in dir(object_name)})
    except:
        for x in dir(object_name):
            try:
                print {x: getattr(object_name, x)}
            except:
                'cant print %s' % x

