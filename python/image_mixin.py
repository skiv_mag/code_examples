

class PhotoExt(object):

    """Mixin for work with image-columns in models in SQLAlchemy"""

    def save_image(self, file_obj, image_col):
        image_folder = os.path.join(app.config['MEDIA_ROOT'], self._meta.name)
        full_path = os.path.join(image_folder, getattr(self, image_col))
        if not os.path.exists(image_folder):
            os.makedirs(image_folder)
        file_obj.save(full_path)
        self.save()

    def url(self, image):
        image_name = getattr(self, image)
        # fix path join for windows
        return os.path.join(
            app.config['MEDIA_URL'],
            self._meta.name,
            image_name
        ).replace('\\', '/')

    def thumb(self):
        image = self.photo_column[0]
        return Markup(
            '<img src="%s" style="height: 80px;" />' % self.url(image)
        )
