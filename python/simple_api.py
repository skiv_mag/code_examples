#!/usr/bin/env python
# -*- coding: utf-8 -*-


class UsersAPI(MethodView):

    def get(self):
        users = as_dict_many(Users.query.all())
        return jsonify(users)

    def post(self):
        allowed_params = ["phone", "facebook_id",  "google_id", "hash_obj"]
        ret_args = filter_params(request.json, allowed_params)

        user = Users.query.filter_by(phone=ret_args["phone"]).first()
        if not user:
            user = Users.create(**ret_args)
        return jsonify(user.as_dict())
