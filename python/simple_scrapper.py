#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2
import urllib2
import base64
import json

from config import Configuration as CONF

POSTGRES_SETTINGS = {
    "host": Configuration.DATABASE["host"],
    "database": Configuration.DATABASE["name"],
    "port": 5433,
    "user": Configuration.DATABASE["user"],
    "password": Configuration.DATABASE["password"]
}

conn = psycopg2.connect(**POSTGRES_SETTINGS)
cur = conn.cursor()


def get_data():
    request = urllib2.Request(CONF.BASE_URL + "/split/snapshot")
    base64string = base64.encodestring(
        '%s:%s' % (CONF.DEFAULT_USER, CONF.DEFAULT_PASS)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request).read()
    return json.loads(result)


def put_data(data):
    cur.execute(
        "create  table if not exists test (\
            id serial, exper text, alter text, part_cnt integer, \
            compl_cnt integer, ctr numeric, tstamp timestamp default now());")
    cur.executemany(
        "INSERT INTO test (exper, alter, part_cnt, compl_cnt, ctr) \
        VALUES (%(exper)s, %(alter)s, %(part_cnt)s, %(compl_cnt)s, %(ctr)s)", data)
    conn.commit()


def main():
    d = get_data()
    put_data(d)
    
    
main()