drop table servicegroup;

create table servicegroup
(
  id serial not null,
  name character varying(255) not null,
  subtitle character varying(255),
  text character varying(255),
  order_id integer,
  constraint servicegroup_pkey primary key (id)
)
;

insert into servicegroup(name)
select distinct "group" from services where "group" != '';

alter table services add column group_id integer;

alter table services add constraint service_group_fk foreign key (group_id)
      references servicegroup (id) match simple
      on update no action on delete no action;

update services
set group_id = servicegroup.id
from servicegroup
where servicegroup.name = services."group";

delete from servicetext
where service_id in(
    select id
    from services
    where name = 'overview'
);

delete from services
where name = 'overview';

update servicegroup
set order_id = 1
where name = 'a';

update servicegroup
set order_id = 2
where name = 'b';

update servicegroup
set order_id = 3
where name = 'c';

alter table services drop column "group";