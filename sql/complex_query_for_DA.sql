select
            m.mand_name,
            ltrim (translate (sum (case when datum = 1 then zinssatz_tagesgeld else 0 end)::text::money::text, ',.','.,'), '$') as y1,
            ltrim (translate (sum (case when datum = 2 then zinssatz_tagesgeld else 0 end)::text::money::text, ',.','.,'), '$') as y2,
            ltrim (translate (sum (case when datum = 3 then zinssatz_tagesgeld else 0 end)::text::money::text, ',.','.,'), '$') as y3,
            ltrim (translate (sum (case when datum = 4 then zinssatz_tagesgeld else 0 end)::text::money::text, ',.','.,'), '$') as y4,
            ltrim (translate (sum (case when datum = 5 then zinssatz_tagesgeld else 0 end)::text::money::text, ',.','.,'), '$') as y5
        from (
            select 
                mandanten_id, extract(isodow from datum)::integer as datum, zinssatz_tagesgeld
            from dwh_actual.zins_ueberregional
            where 1=1
                and datum between 
                            current_date - (extract (isodow from now())-1||'day')::interval
                            and 
                            current_date + (7- extract (isodow from now())||'day')::interval
                and mandanten_id in (
                                select 
                                    regexp_split_to_table (cco.list_competitors,e'\\\\s+')
                                from dwh_actual.conditions_customer_old cco
                                    inner join (
                                            select 
                                                id_mand, max(tstamp) as tstamp 
                                            from dwh_actual.conditions_customer_old
                                            where id_mand      = $1
                                            group by id_mand
                                    ) as s
                                       on      cco.id_mand     = s.id_mand 
                                           and cco.tstamp      = s.tstamp
                )
                and zinssatz_tagesgeld is not null
            order by mandanten_id
        ) as t
            inner join dwh_actual.mandanten m
                on m.id_mand = t.mandanten_id
        group by m.mand_name
        order by m.mand_name;                        